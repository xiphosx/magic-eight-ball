var answers = [
    'It is certain', 'It is decidedly so', 'Reply hazy try again', 'Cannot predict now', 'Do not count on it', 'My sources say no', 'Outlook not so good', 'Signs point to yes' ];

var getResponse = function() {
    return answers[Math.floor(Math.random() * answers.length)];
};

$(document).on('keypress', function(e) {
    if(e.which == 13) { 
        $('#hero-wrapper').effect("shake");
        $('#response p').html(getResponse());
        e.preventDefault();
    };
});
